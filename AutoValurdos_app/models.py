from django.db import models

class Estuiantes(models.Model):
    nombre = models.CharField(max_length=20)
    codigo = models.IntegerField()
    fecha = models.DateField()
    direccion = models.CharField(max_length=100)
    telefono = models.IntegerField()
    celular = models.BigIntegerField()
    email = models.EmailField()

    def __str__(self):
        return self.nombre
