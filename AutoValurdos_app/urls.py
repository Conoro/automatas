from django.urls import path

from AutoValurdos_app.views import HomeView, DownloadTemplate, ImporterView

urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('download', DownloadTemplate.as_view(), name="download"),
    path('importer', ImporterView.as_view(), name="importer"),
]