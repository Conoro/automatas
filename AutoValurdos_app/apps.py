from django.apps import AppConfig


class AutovalurdosAppConfig(AppConfig):
    name = 'AutoValurdos_app'
