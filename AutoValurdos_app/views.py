import os

from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
import re

from django.views.generic import FormView

from AutoValurdos_app.models import Estuiantes


class HomeView(View):
    def get(self, request):
        return render(request, "home.html")

    def post(self, request, *args, **kwargs):
        name = self.request.POST["name"]
        code = self.request.POST["codigo"]
        fecha = self.request.POST["fecha"]
        direccion = self.request.POST["direccion"]
        telefono = self.request.POST["telefono"]
        cell = self.request.POST["cell"]
        email = self.request.POST["email"]
        val = str(cell)
        val = val.split()
        validateName = re.match("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", name, re.I)
        validateCode = re.match("^[1-9][0-9]{7}$", code, re.I)
        validateFecha = re.match("^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$", fecha, re.I)
        validateDireccion = re.match("[#--]", direccion, re.I)
        validateTelefono = re.match("^[1-9][0-9]{6}$", telefono, re.I)
        validateCell = re.match("^[3-3][0-9]{9}$", cell, re.I)
        validateEmail = re.match("^w+([.-]?w+)+@w+([.:]?w+)+(.[a-zA-Z0-9]{2,3})+$", email, re.I)
        html = "home.html"
        try:
            immporting = self.request.POST['import']
            if immporting == "import":
                data = """ <?xml version="1.0" encoding="ISO-8859-1"?>
    
                """
                personas = Estuiantes.objects.all()
                for item in personas:
                    data = data + """
                    <ESTUDIANTE>
                <NOMBRE> nombre </NOMBRE>
                <CODIGO> codigo </CODIGO>
                <FECINGRESO> fecha </FECINGRESO>
                <DIRECCION> direccion </DIRECCION >
                <TELEFONOFIJO> telefono </TELEFONOFIJO >
                <CELULAR>cell</CELULAR >
                <EMAIL> correo </EMAIL >
                </ESTUDIANTE> """
                    data = data.replace("nombre", item.nombre)
                    data = data.replace("codigo", str(item.codigo))
                    data = data.replace("fecha", str(item.fecha))
                    data = data.replace("direccion", str(item.direccion))
                    data = data.replace("telefono", str(item.telefono))
                    data = data.replace("cell", str(item.celular))
                    data = data.replace("correo", str(item.email))

                with open("media/archive_prueba.xml", 'w') as file:
                    file.write(data)
                    file.close()
                context = {'link': "media/archive_prueba.xml"}
                return render(request, "download.html", context)

        except:
            val = str(cell).split()
            if not validateTelefono and not validateName and not validateCode and not validateFecha \
                    and not validateDireccion and not validateCell and not validateEmail \
                    and (len(val) == 0 or val[0] != "3" ):
                print("esa verga no coincide")
                context = {'error': 'Verifique los datos y cuelva a llenar el formulario'}
                return render(request, "home.html", context)
            else:
                html = "home.html"
                code = int(code)
                Estuiantes.objects.create(nombre=name, codigo=code, fecha=fecha, direccion=direccion,
                                          telefono=int(telefono),
                                          celular=int(cell), email=email)
        return render(request, html)


class DownloadTemplate(View):
    def get(self, request):
        context = {'link': "media/archive.xml"}
        return render(request, "download.html", context)


class ImporterView(View):
    def get(self, request):
        return render(request, "importer.html")

    def post(self, request):
        archive = request.FILES['archive']
        fs = FileSystemStorage()
        fs.save(archive.name, archive)
        name = f"media/{archive.name}"  # esta es la direccion para abrir el archivo
        document = open(name).read()
        Name = re.findall('<NOMBRE>(.*)</NOMBRE>', document)
        code = re.findall('<CODIGO>(.*)</CODIGO>', document)
        date = re.findall('<FECINGRESO>(.*)</FECINGRESO>', document)
        address = re.findall('<DIRECCION>(.*)</DIRECCION>', document)
        phone = re.findall('<TELEFONOFIJO>(.*)</TELEFONOFIJO>', document)
        cell = re.findall('<CELULAR>(.*)</CELULAR>', document)
        email = re.findall('<EMAIL>(.*)</EMAIL>', document)
        os.remove(name)
        code = int(code[0])
        phone = int(phone[0])
        cell = int(cell[0])
        Estuiantes.objects.create(nombre=Name[0], codigo=code, fecha=date[0], direccion=address[0],
                                  telefono=phone,
                                  celular=cell, email=email[0])
        return render(request, "home.html")
